package com.thien.repository;

import java.util.List;

import com.thien.entities.Order;

public interface OrderRepo extends AbstractRepo<Integer, Order>{
	List<String[]> thongKeBanHang(String tuNgay,String denNgay);
}
