package com.thien.repository.impl;

import org.springframework.stereotype.Repository;

import com.thien.entities.User;
import com.thien.repository.UserRepo;
@Repository
public class UserRepoImpl extends AbstractRepoImpl<Integer, User> implements UserRepo {

}
