package com.thien.repository.impl;

import org.springframework.stereotype.Repository;

import com.thien.entities.Role;
import com.thien.repository.RoleRepo;

@Repository
public class RoleRepoImpl extends AbstractRepoImpl<Integer, Role> implements RoleRepo {

}
