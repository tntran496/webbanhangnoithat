package com.thien.repository.impl;

import org.springframework.stereotype.Repository;

import com.thien.entities.Category;
import com.thien.repository.CategoryRepo;
@Repository
public class CategoryRepoImpl extends AbstractRepoImpl<Integer, Category> implements CategoryRepo {

}
