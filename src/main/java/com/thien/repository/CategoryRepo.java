package com.thien.repository;

import com.thien.entities.Category;

public interface CategoryRepo extends AbstractRepo<Integer, Category>{

}
