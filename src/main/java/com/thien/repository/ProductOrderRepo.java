package com.thien.repository;

import com.thien.entities.ProductOrder;

public interface ProductOrderRepo extends AbstractRepo<Integer, ProductOrder>{
	 boolean deleteProductOrder(ProductOrder productOrder);
}
