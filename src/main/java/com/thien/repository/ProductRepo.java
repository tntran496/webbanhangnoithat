package com.thien.repository;

import java.util.List;

import com.thien.entities.Product;

public interface ProductRepo extends AbstractRepo<Integer,Product>{
	List<Product> getListSpKhuyenMai(int offset,int limit);
	int getCountSpKhuyenMai();
}
