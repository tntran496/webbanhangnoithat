package com.thien.controller.web;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.thien.dto.OrderDto;
import com.thien.dto.ProductDto;
import com.thien.dto.ProductOrderDto;
import com.thien.dto.SessionGioHang;
import com.thien.dto.SessionUser;
import com.thien.dto.UserDto;
import com.thien.interceptor.Auth;
import com.thien.interceptor.Auth.Role;
import com.thien.service.OrderService;
import com.thien.service.ProductOrderService;
import com.thien.service.ProductService;
import com.thien.service.UserService;

@Controller
public class CartController {
	@Autowired
	ProductService productService;

	@Autowired
	ProductOrderService productOrderService;

	@Autowired
	OrderService orderService;

	@Autowired
	UserService userService;

	@Autowired
	JavaMailSender mailSender;

	@RequestMapping(path = "/cart", method = RequestMethod.GET)
	public String viewCartProduct(ModelMap map, HttpSession session,@RequestParam(required = false) String message) {
		List<ProductDto> listSpDto = new ArrayList<ProductDto>();
		SessionGioHang gioHang = (SessionGioHang) session.getAttribute("gioHang");
		if (gioHang != null) {
			for (ProductOrderDto productOrderDto : gioHang.getGioHangs()) {
				ProductDto dto = productService.findProductByIdService(productOrderDto.getIdProduct());
				dto.setTotalItem(productOrderDto.getCount());
				dto.setTotalPrices(productOrderDto.getPrice());
				listSpDto.add(dto);
			}

		}
		map.addAttribute("message",message);
		map.addAttribute("orderdto", new OrderDto());
		map.addAttribute("productsCart", listSpDto);
		return "web/giohang";
	}

	@RequestMapping(path = "/api/v1/cart", method = RequestMethod.POST)
	@ResponseBody
	public String themSanPhamVaoGioHang(@RequestBody ProductOrderDto productOrderDto, HttpSession session) {
		boolean checkTonTai = false;
		int existProduct = -1;
		int vtExits = -1;
		long total = 0;
		int totalItem = 0;
		SessionGioHang gioHang = (SessionGioHang) session.getAttribute("gioHang");
		ProductDto productDto = productService.findProductByIdService(productOrderDto.getIdProduct());

		if (productDto.getCount() < productOrderDto.getCount()) {
			return "fail";
		}
		for (int i = 0; i < gioHang.getGioHangs().size(); i++) {
			if (productOrderDto.getIdProduct() == gioHang.getGioHangs().get(i).getIdProduct()) {
				checkTonTai = true;
				existProduct = gioHang.getGioHangs().get(i).getCount();
				vtExits = i;
				break;
			}
		}

		if (checkTonTai) {
			productOrderDto.setCount(productOrderDto.getCount() + existProduct);
			productOrderDto.setPrice(
					(long) ((productOrderDto.getPrice() - productOrderDto.getPrice() * (productDto.getDiscount() / 100))
							* productOrderDto.getCount()));
			gioHang.getGioHangs().set(vtExits, productOrderDto);
			gioHang.setGioHangs(gioHang.getGioHangs());
			for (ProductOrderDto dto : gioHang.getGioHangs()) {
				// total
				// +=productService.findProductByIdService((Integer)dto.getIdProduct()).getPrice()*dto.getCount();
				total += dto.getPrice();
				totalItem += dto.getCount();
			}
			NumberFormat nf = NumberFormat.getInstance(new Locale("en", "US"));
			gioHang.setTotalPrice(nf.format(total));
			gioHang.setTotalItem(totalItem);
		} else {
			productOrderDto.setPrice(
					(long) ((productOrderDto.getPrice() - productOrderDto.getPrice() * (productDto.getDiscount() / 100))
							* productOrderDto.getCount()));
			gioHang.getGioHangs().add(productOrderDto);
			gioHang.setGioHangs(gioHang.getGioHangs());

			for (ProductOrderDto dto : gioHang.getGioHangs()) {
				// total
				// +=productService.findProductByIdService((Integer)dto.getIdProduct()).getPrice()*dto.getCount();
				total += dto.getPrice();
				totalItem += dto.getCount();
			}
			NumberFormat nf = NumberFormat.getInstance(new Locale("en", "US"));
			gioHang.setTotalPrice(nf.format(total));
			gioHang.setTotalItem(totalItem);
		}

		return "success";
	}

	@RequestMapping(path = "/api/v1/cart/{idProduct}", method = RequestMethod.DELETE)
	@ResponseBody
	public String xoaSanPhamRaGioHang(@PathVariable int idProduct, HttpSession session) {
		SessionGioHang gioHang = (SessionGioHang) session.getAttribute("gioHang");
		ProductOrderDto spXoa = null;
		for (ProductOrderDto dto : gioHang.getGioHangs()) {
			if (dto.getIdProduct() == idProduct) {
				spXoa = dto;
				break;
			}
		}
		gioHang.getGioHangs().remove(spXoa);
		gioHang.setGioHangs(gioHang.getGioHangs());
		long total = 0;
		int totalItem = 0;
		for (ProductOrderDto dto : gioHang.getGioHangs()) {
			total += dto.getPrice();
			totalItem += dto.getCount();
		}
		NumberFormat nf = NumberFormat.getInstance(new Locale("en", "US"));
		gioHang.setTotalPrice(nf.format(total));
		gioHang.setTotalItem(totalItem);
		return "success";
	}

	@Auth(role = Role.LOGIN)
	@RequestMapping(path = "/order", method = RequestMethod.POST)
	public String insertOrder(ModelMap map,HttpSession session, HttpServletRequest request,@Validated @ModelAttribute("orderdto") OrderDto orderdto,BindingResult bindingResult) {
		SessionGioHang gioHang = (SessionGioHang) session.getAttribute("gioHang");
		if(bindingResult.hasErrors()) {
			List<ProductDto> listSpDto = new ArrayList<ProductDto>();
			if (gioHang != null) {
				for (ProductOrderDto productOrderDto : gioHang.getGioHangs()) {
					ProductDto dto = productService.findProductByIdService(productOrderDto.getIdProduct());
					dto.setTotalItem(productOrderDto.getCount());
					dto.setTotalPrices(productOrderDto.getPrice());
					listSpDto.add(dto);
				}

			}
			map.addAttribute("message","");
			map.addAttribute("order", new OrderDto());
			map.addAttribute("productsCart", listSpDto);
			return "web/giohang";
		}
		if(gioHang.getGioHangs().size()<=0) {
			return "redirect:/home";
		}
		
		try {
			
			NumberFormat nf = NumberFormat.getInstance(new Locale("en", "US"));
			StringBuilder sendText = new StringBuilder();
			String from = "tranducthien12699@gmail.com";
			List<ProductDto> listProduct = new ArrayList<ProductDto>();
			
			//format mail
			sendText.append("<style type=\"text/css\">\n" + "table {\n" + "	border-collapse: collapse;\n"
					+ "	width: 100%;\n" + "	}\n" + "	th,td{\n" + "		line-height: 25px;\n"
					+ "		border: 1px solid black;\n" + "		padding:5px;\n" + "	}\n" + "	th{\n"
					+ "		background-color: gray;\n" + "	}\n" + "	</style>");
			sendText.append(
					"<p>Cảm ơn bạn đã mua hàng ở NoiThatThien ! Đơn hàng của bạn sẽ dược giao trong 2-3 ngày !Đây là hóa đơn giao hàng của bạn : </p>");
			sendText.append("<h1 style=" + "color:blue;" + ">Hóa đơn</h1>");
			//
			
			MimeMessage mail = mailSender.createMimeMessage();
			MimeMessageHelper helper = new MimeMessageHelper(mail);
			
			SessionUser sessionUser = (SessionUser) session.getAttribute("sessionUser");
			UserDto userDtoMail = userService.findByIdUserService(sessionUser.getUserId());
			UserDto userDto = new UserDto();

			orderdto.setTotalMoney(Long.parseLong(gioHang.getTotalPrice().replace(",", "")));
			orderdto.setStatus(false);
			userDto.setId(sessionUser.getUserId());
			orderdto.setUserDto(userDto);
			
			orderdto = orderService.saveOderService(orderdto);
			
			//format mail
			sendText.append("<p> Số hóa đơn: " + orderdto.getId() + "</p>");
			sendText.append("<p> Ngày đặt: " + orderdto.getCreatedDate() + "</p>");
			sendText.append("<p> Người đặt : " + userDtoMail.getFullName() + "</p>");
			sendText.append("<p> Người gửi : " + orderdto.getReceiver() + "</p>");
			sendText.append("<p> Địa chỉ giao hàng : " + orderdto.getDeliveryAddress() + "</p>");
			sendText.append("<p> Số điện thoại nhận : " + orderdto.getPhone() + "</p>");
			sendText.append("<p> Nội dung : " + orderdto.getContent() + "</p>");
			sendText.append("<table><tr>");
			sendText.append("<th>Mã</th>").append("<th>Tên sản phẩm</th>").append("<th>Giá</th>")
					.append("<th>Giảm giá</th>").append("<th>Số lượng</th>").append("<th>Tổng tiền</th>");
			sendText.append("</tr>");
			//
			
			//kiem tra so luong hang co phu hop voi kho hay ko
			for (ProductOrderDto po : gioHang.getGioHangs()) {
				ProductDto productDto = productService.findProductByIdService(po.getIdProduct());
				if(productDto.getCount()>=po.getCount()) {
					productDto.setCount(productDto.getCount() -po.getCount());
					listProduct.add(productDto);
					
					//format mail
					sendText.append("<tr>");
					sendText.append("<td>").append(productDto.getId().intValue()).append("</td>");
					sendText.append("<td>").append(productDto.getName()).append("</td>");
					sendText.append("<td>").append(nf.format(productDto.getPrice().longValue())).append("</td>");
					sendText.append("<td>").append(productDto.getDiscount()).append("</td>");
					sendText.append("<td>").append(po.getCount()).append("</td>");
					sendText.append("<td>").append(nf.format(po.getPrice().longValue())).append("</td>");
					sendText.append("</tr>");
					//
				}else {
					orderService.deleteOrderService(orderdto.getId());
					map.addAttribute("message","Sản phẩm "+productDto.getName()+" đã hết hàng");
					return "redirect:/cart";
				}
			
			}
			//format mail
			sendText.append("</table>");
			sendText.append("<p style='color:red;'> Tổng tiền : ").append(gioHang.getTotalPrice()).append(" VND")
					.append("</p>");
			//
			int i = 0;
			for(ProductDto pDto : listProduct ) {
				gioHang.getGioHangs().get(i).setIdOrder(orderdto.getId());
				productOrderService.saveProductOderService(gioHang.getGioHangs().get(i));
				productService.saveProductService(pDto);
				i++;
			}
			
			helper.setFrom(from, from);
			helper.setTo(userDtoMail.getEmail());
			helper.setReplyTo(from, from);
			helper.setSubject("Thank You !");
			helper.setText(sendText.toString(), true);

			mailSender.send(mail);

		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e.getMessage());
		} 
		session = request.getSession(false);
		session.removeAttribute("gioHang");
		return "redirect:/home";

	}
}
