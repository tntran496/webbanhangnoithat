package com.thien.service;

import java.util.List;

import com.thien.dto.RoleDto;

public interface RoleService {
	List<RoleDto> getListRoleService();
	RoleDto getRoleById(int id);
}
