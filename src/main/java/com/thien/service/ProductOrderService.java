package com.thien.service;

import java.util.List;

import com.thien.dto.ProductOrderDto;

public interface ProductOrderService {
	void saveProductOderService(ProductOrderDto productOrderDto);
	List<ProductOrderDto> getListProductOrderDtoByOrder(Integer idOrder);
}
